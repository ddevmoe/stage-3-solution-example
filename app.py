from fastapi import FastAPI, WebSocket

from models import PrivateSessionDispatcher, User


app = FastAPI()
dispatcher = PrivateSessionDispatcher()


@app.websocket('/private-session')
async def private_session(websocket: WebSocket, initiator: str, target: str):
    await websocket.accept()
    user = User(initiator, websocket)
    session = await dispatcher.get_session(user, target)
    if not session:
        await websocket.send_text('System: Idle session disconnected')
    else:
        await session.participate(user)
