import asyncio
from dataclasses import dataclass
from datetime import datetime

from starlette.websockets import WebSocket, WebSocketDisconnect, WebSocketState


@dataclass
class User:
    name: str
    websocket: WebSocket


class PrivateSession:
    IDLE_SESSION_TRESHOLD_SECONDS = 20
    IDLE_SESSION_PROBE_INTERVAL_SECONDS = 5

    def __init__(self):
        self._participants: list[User] = []
        self._idle_session_counter_seconds = 0
        self._is_active = True
        self._idle_session_worker_active = False

    def _reset_idle_session_counter(self):
        self._idle_session_counter_seconds = self.IDLE_SESSION_TRESHOLD_SECONDS

    async def _safe_close_connection(self, connection: WebSocket):
        try:
            if connection.client_state != WebSocketState.DISCONNECTED:
                await connection.close()
        except (WebSocketDisconnect, RuntimeError) as _error:
            pass

    async def _idle_session_termination_worker(self):
        if self._idle_session_worker_active:
            return

        self._idle_session_worker_active = True
        self._reset_idle_session_counter()
        while self._is_active and self._idle_session_counter_seconds > 0:
            await asyncio.sleep(self.IDLE_SESSION_PROBE_INTERVAL_SECONDS)
            self._idle_session_counter_seconds -= self.IDLE_SESSION_PROBE_INTERVAL_SECONDS

        await self.cleanup()

    async def _broadcast_message(self, message: str, sender: User):
        """
        Sends `message` with appended timestamp and `sender.name` to all connected users other than `sender`
        """
        message_time_portion = datetime.now().strftime('%H:%M:%S')
        formatted_message = f'[{message_time_portion}] {sender.name}: {message}'

        await asyncio.gather(*[
            user.websocket.send_text(formatted_message)
            for user in self._participants
            if user != sender
        ])

    async def _play_session(self, user: User):
        while self._is_active:
            message = await user.websocket.receive_text()
            self._reset_idle_session_counter()
            await self._broadcast_message(message, user)

    def join_user(self, user: User):
        self._participants.append(user)

    async def cleanup(self):
        """
        Gracefully disconnects all connected users and stops the session.
        """

        # Safe switch so cleanup will play only once per session, as it can be invoked from multiple locations
        if not self._is_active:
            return

        self._is_active = False
        await asyncio.gather(*[
            self._safe_close_connection(user.websocket)
            for user in self._participants
        ])

    async def participate(self, user: User):
        # Dispatch background task to terminate session when considered inactive
        asyncio.ensure_future(self._idle_session_termination_worker())

        try:
            await self._play_session(user)
        except WebSocketDisconnect:
            await self.cleanup()


class PrivateSessionDispatcher:
    """
    Class responsible for merging two clients requesting a one-on-one live chatting session.

    The class assists in waiting for both parties to connect with each other and create a ready to use `PrivateSession` instance.

    Multiple separate sessions between the same two parties CAN be created using this class as it only 'dispatches' them, and does not keep them in it's state.
    Same initiator-target requests CAN be made and will result in a session of a single username with two sockets.
    """

    IDLE_PENDING_SESSION_TIMEOUT_SECONDS = 10
    IDLE_PENDING_SESSION_PROBE_INTERVAL_SECONDS = 1

    def __init__(self):
        self._pending_session_keys: list[str] = []
        self._session_by_session_key: dict[str, PrivateSession] = {}

    def _generate_session_key(self, names: tuple[str, str]) -> str:
        """
        Returns a session key consisting of participants' names.
        Same name pairs will generate the same key - order insensitive.
        """

        return ';'.join(sorted(names))

    async def _on_first_user_connected(self, session_key: str, initiator: User) -> 'PrivateSession | None':
        self._pending_session_keys.append(session_key)

        idle_pending_session_counter_seconds = self.IDLE_PENDING_SESSION_TIMEOUT_SECONDS
        while (
            session_key not in self._session_by_session_key
            and idle_pending_session_counter_seconds > 0
        ):
            await asyncio.sleep(self.IDLE_PENDING_SESSION_PROBE_INTERVAL_SECONDS)
            idle_pending_session_counter_seconds -= self.IDLE_PENDING_SESSION_PROBE_INTERVAL_SECONDS

        self._pending_session_keys.remove(session_key)

        # We pop the session from created sessions to 'clean' the dispatcher and allow recreation of the same session
        session = self._session_by_session_key.pop(session_key, None)
        if session:
            session.join_user(initiator)

        return session

    async def _on_second_user_conencted(self, session_key: str, initiator: User) -> PrivateSession:
        session = PrivateSession()
        self._session_by_session_key[session_key] = session
        session.join_user(initiator)
        return session

    async def get_session(self, initiator: User, target_name: str) -> 'PrivateSession | None':
        """
        Blocks asynchronously until a session can be created.

        Returns a `PrivateSession` instance on success (when both parties connect successfully).

        Returns `None` when a predefined timeout is reached.
        """

        session_key = self._generate_session_key((initiator.name, target_name))

        # If a session key exists in `self._pending_session_keys`, it means the first user already connected
        # and we now handle the second user connection
        if session_key in self._pending_session_keys:
            return await self._on_second_user_conencted(session_key, initiator)

        return await self._on_first_user_connected(session_key, initiator)
